import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class BookController {

    @RequestMapping("/books")

    @GetMapping
    public List<Book> all() {

        return List.of(
                new Book("Anansi Boys", "Neil Gaiman"),
                new Book("The Martian", "Andy Weir"));
    }
}
